
//グローバル変数
float4x4 _matWVP;
float4x4 _matW;
float4x4 _matR;
float3	 _Light;


float4	_DiffuseColor;
float4	_AmbientColor;
float4	_SpecularColor;

float	_SpeSize;

float3	_CameraPos;	//カメラの位置
bool	_isTexture;
bool	_isNormalMap;
texture		_tex;
texture _normalTex;

sampler Sampler = sampler_state
{
	Texture = <_tex>;
};

sampler SamplerNormal = sampler_state
{
	Texture = <_normalTex>;
};


struct VS_OUT
{
	float4 P : SV_POSITION;
	float3 E : TEXCOORD2;
	float2 uv :	TEXCOORD0;
	float3 L: TEXCOORD3;
	float3 N: NORMAL;
};



//頂点シェーダー
VS_OUT VS(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD0, float3 tangent : TANGENT)
{
	VS_OUT outData;
	outData.P = mul(pos, _matWVP);


	float3 binormal = cross(tangent, normal);

	normal = normalize(mul(normal, _matR));
	tangent = normalize(mul(tangent, _matR));
	binormal = normalize(mul(binormal, _matR));

	//ノーマルマップが無いときのために、法線情報をピクセルシェーダに渡す
	outData.N = normal;


	float4 worldPos = mul(pos, _matW);

		float3 eye = normalize(_CameraPos - worldPos);

	//法線マップあり
	if (_isNormalMap)
	{
		
			outData.E.x = dot(eye, tangent);
		outData.E.y = dot(eye, binormal);
		outData.E.z = dot(eye, normal);


		float3 light = _Light;
			outData.L.x = dot(light, tangent);
		outData.L.y = dot(light, binormal);
		outData.L.z = dot(light, normal);
	}

	//法線マップ無し
	else
	{
		outData.E = eye;
		outData.L = _Light;
	}

	outData.uv = uv;
	return outData;
}


//ピクセルシェーダー
float4 PS(VS_OUT inData) : COLOR
{
	float3 L = normalize(inData.L);


	float3 N;
	if (_isNormalMap)	//ノーマルマップがある場合は、それを元に法線を決める
		N = 2.0f * tex2D(SamplerNormal, inData.uv) - 1.0f;
	else				//無い場合は頂点シェーダから受け取った法線情報をそのまま使う
		N = inData.N;

	float3 V = normalize(inData.E);

	float4 c;
	if (_isTexture)
	{
		c = tex2D(Sampler, inData.uv);
	}
	else
	{
		c = _DiffuseColor;
	}

	float4 diffuse = saturate(dot(N, -L)) * c;



		float3 R = reflect(L, N);	//反射ベクトル
		float spePower =2.0f;													//ハイライトの強さ

	float4 Specular = pow(saturate(dot(R, V)), _SpeSize) * spePower *_SpecularColor;	//鏡面反射光

		float4 color = diffuse + Specular + _AmbientColor;	//最終的な色
		return color;
}

//頂点シェーダーとピクセルシェーダーの組み合わせ
technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}

