#pragma once
#include "MyGameEngine\Fbx.h"
class Tank : public Fbx
{
	const float RUN_SPEED;
	const float ROTATE_SPEED;
	BOOL		_isWallHit;
	D3DXVECTOR3	_wallNormal;

public:
	Tank();
	~Tank();

	void init();
	void input() override;

	CREATE_FUNC(Tank);

	void collisionGround(Fbx* ground);
	void collisionWall(Fbx* wall);
};

