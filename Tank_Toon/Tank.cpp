#include "Tank.h"


Tank::Tank() :
RUN_SPEED(0.1f), 
ROTATE_SPEED(1.0f)
{
	
}


Tank::~Tank()
{
}

void Tank::init()
{
	load("Assets\\tank.fbx");
}

void Tank::input()
{
	D3DXVECTOR3 move(0, 0, RUN_SPEED);

	D3DXMATRIX rotateMatrix;
	D3DXMatrixRotationY(&rotateMatrix, D3DXToRadian(_rotate.y));
	D3DXVec3TransformCoord(	&move, &move, &rotateMatrix);

	if (g.pInput->isKeyPush(DIK_UP))
	{
		if (_isWallHit == FALSE)
		{
			_position += move;
		}
		else
		{
			_position += move - _wallNormal * D3DXVec3Dot(&move, &_wallNormal);
		}
	}

	if (g.pInput->isKeyPush(DIK_DOWN))
	{
		_position -= move;
	}

	if (g.pInput->isKeyPush(DIK_RIGHT))
	{
		_rotate.y += ROTATE_SPEED;
	}

	if (g.pInput->isKeyPush(DIK_LEFT))
	{
		_rotate.y -= ROTATE_SPEED;
	}


}

void Tank::collisionGround(Fbx* ground)
{
	RayCastData data;

	data.start = _position;
	data.start.y = 0;
	data.dir = D3DXVECTOR3(0, -1, 0);

	ground->rayCast(&data);
	if (data.hit == TRUE)
	{
		_position.y = -data.dist;
	}

}

void Tank::collisionWall(Fbx* wall)
{
	RayCastData data;

	data.start = _position;
	data.dir = D3DXVECTOR3(0, 0, 1);
	D3DXMATRIX rotate;
	D3DXMatrixRotationY(&rotate, D3DXToRadian(_rotate.y));
	D3DXVec3TransformCoord(&data.dir, &data.dir, &rotate);

	wall->rayCast(&data);
	if (data.hit == TRUE && data.dist <= 3 )
	{
		_isWallHit = TRUE;
		_wallNormal = data.normal;
	}
	else
	{
		_isWallHit = FALSE;
	}

}