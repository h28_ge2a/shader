#include "MyRect.h"

//コンストラクタ（引数なし）
MyRect::MyRect()
{
	_left = 0.0f;
	_top = 0.0f;
	_width = 0.0f;
	_height = 0.0f;
}

//コンストラクタ（引数あり）
MyRect::MyRect(float left, float top, float width, float height)
{
	_left = left;
	_top = top;
	_width = width;
	_height = height;
}

//デストラクタ
MyRect::~MyRect()
{
}

//衝突判定
bool MyRect::intersectsRect(MyRect targetRect)
{
	if (_left + _width > targetRect._left && _left < targetRect._left + targetRect._width
		&& _top + _height > targetRect._top && _top < targetRect._top + targetRect._height)
	{
		return true;
	}

	return false;
}
