#include "Node.h"
#include "Scene.h"

Node::Node()

{
	_size = D3DXVECTOR3(0, 0, 0);
	_anchorPoint = D3DXVECTOR3(0.5, 0.5, 0);
	_position = D3DXVECTOR3(0, 0, 0);
	_rotate = D3DXVECTOR3(0, 0, 0);
	_scale = D3DXVECTOR3(1, 1, 1);
	_parent = nullptr;
	_tag = "";
}

Node::~Node()
{

	
}

void Node::setAnchorPoint(float x, float y, float z)
{
	_anchorPoint.x = x;
	_anchorPoint.y = y;
	_anchorPoint.z = z;
}


void Node::setPosition(float x, float y, float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;
}

void Node::setPosition(D3DXVECTOR3 pos)
{
	setPosition(pos.x, pos.y, pos.z);
}

void Node::setRotate(float x, float y, float z)
{
	_rotate.x = x;
	_rotate.y = y;
	_rotate.z = z;
}


void Node::setRotate(float z)
{
	setRotate(0, 0, z);
}


void Node::setScale(float x, float y, float z)
{
	_scale.x = x;
	_scale.y = y;
	_scale.z = z;
}

void  Node::setParent(Scene* parent)
{
	_parent = parent;
}

void Node::setTag(std::string tag)
{
	_tag = tag;
}

D3DXVECTOR3 Node::getSize()
{
	D3DXVECTOR3 size = _size;
	size.x *= _scale.x;	
	size.y *= _scale.y;
	size.z *= _scale.z;
	return size;
}

D3DXVECTOR3 Node::getAnchorPoint()
{
	return _anchorPoint;	
}

D3DXVECTOR3 Node::getPosition()
{
	return _position;
}


D3DXVECTOR3 Node::getRotate()
{
	return _rotate;
}

D3DXVECTOR3 Node::getScale()
{
	return _scale;
}

std::string Node::getTag()
{
	return _tag;
}


//�Փ˔���͈�
MyRect Node::getBoundingBox()
{
	MyRect rect;

	rect._left =	_position.x - getSize().x * _anchorPoint.x;
	rect._top =		_position.y - getSize().y * _anchorPoint.y;
	rect._width =	_size.x;
	rect._height =	_size.y;

	return rect;
}

//���g���폜
void Node::removeFromParent()
{
	_parent->removeChild(this);
}