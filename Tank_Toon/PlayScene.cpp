#include "PlayScene.h"

#include "MyGameEngine\Fbx.h"
#include "MyGameEngine\Light.h"
#include "MyGameEngine\Camera.h"
#include "Tank.h"
#include "MyGameEngine\Quad.h"

PlayScene::PlayScene()
{

}


PlayScene::~PlayScene()
{

}

void PlayScene::init()
{
	auto light = Light::create();
	this->addChild(light);

	auto ground = Fbx::create("Assets\\ground.fbx");
	this->addChild(ground);
	_ground = ground;
	ground->setRotate(0, 90, 0);

	auto wall = Fbx::create("Assets\\wall.fbx");
	this->addChild(wall);
	_wall = wall;

	auto tank = Tank::create();
	this->addChild(tank);
	_tank = tank;

}


void PlayScene::update()
{
	Scene::update();

	_camera->setTarget(_tank->getPosition());

	D3DXVECTOR3 camVec(0, 5, -10);
	D3DXMATRIX rotateMatrix;
	D3DXMatrixRotationY(&rotateMatrix, D3DXToRadian(_tank->getRotate().y));
	D3DXVec3TransformCoord(
		&camVec, &camVec, &rotateMatrix);


	_camera->setPosition(
		_tank->getPosition() + camVec);


	_tank->collisionGround(_ground);
	_tank->collisionWall(_wall);

}

void PlayScene::input()
{
	Scene::input();

}