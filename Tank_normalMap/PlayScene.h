#pragma once
#include "MyGameEngine\Scene.h"

class Tank;
class PlayScene :public Scene
{
	Tank*	_tank;
	Fbx*	_ground;
	Fbx*	_wall;

public:
	PlayScene();
	~PlayScene();
	void init()   override;
	void update() override;
	void input() override;
};

