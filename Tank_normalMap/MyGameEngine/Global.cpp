#include "Global.h"
#include "Scene.h"


//シーン切り替え
//引数：pNextScene	次のシーンのオブジェクト
void Global::replaceScene(Scene* pNextScene)
{
	delete pScene; //現在のシーンを開放

	pScene = pNextScene;	//シーンを変えて
	pScene->init();			//新しいシーンの初期化

}